package main

import (
	"log"

	fiber "github.com/gofiber/fiber/v2"
	pion "github.com/pion/webrtc/v3"
	"gitlab.com/leemkh1993/webrtc-streamer/helper"
	WebRtcClient "gitlab.com/leemkh1993/webrtc-streamer/webrtc-client"
)

var (
	streamer       string                                = "http://localhost:10000"
	runningClients map[string]*WebRtcClient.WebRtcClient = make(map[string]*WebRtcClient.WebRtcClient)
)

func handler(c *fiber.Ctx) error {
	// Retrieve url
	rtspUrl := c.Query("rtspUrl")
	if len(rtspUrl) == 0 {
		return c.SendString("RTSP url is not found.")
	} else if len(rtspUrl) > 0 && rtspUrl[:4] != "rtsp" {
		return c.SendString("URL with RTSP protocol is required.")
	}

	// Setup streamer client
	client := WebRtcClient.New(streamer, rtspUrl)
	// Create PC
	err := client.CreatePeerConnection()

	helper.IsError("Create Peer Connection Error: ", err)

	// Create offer
	offer, err := client.CreateOffer()
	if err != nil {
		log.Fatal("Create offer error: ", err)
	}
	// Create offer to local description
	if err := client.SetOffer(offer); err != nil {
		log.Fatal(err)
	}

	// Set offer to streamer
	callApi := helper.GetApi(client.StreamerUrl, "call", client.PeerId, client.Url)
	answer := helper.SendOfferToStreamer(callApi, offer)
	// Set answer to remote description
	if err := client.SetAnswer(answer); err != nil {
		log.Fatal("Set answer error: ", err)
	}

	// Store client to background
	runningClients[client.PeerId] = &client
	return c.SendString("Assigned peer id: " + client.PeerId)
}

func getAllClients(ctx *fiber.Ctx) error {
	return ctx.JSON(runningClients)
}

func getClient(ctx *fiber.Ctx) error {
	peerId := ctx.Params("peerId")
	client := runningClients[peerId]
	if client == nil {
		return ctx.SendString("Client not found")
	}

	json := make(map[string]*pion.SessionDescription)

	json["offer"] = client.Pc.CurrentLocalDescription()
	json["answer"] = client.Pc.CurrentRemoteDescription()

	log.Println(client.VideoTrack)
	log.Println("let try")

	return ctx.JSON(json)
}

func main() {
	app := fiber.New()

	app.Get("/getAllClients", getAllClients)
	app.Get("/getClient/:peerId", getClient)
	app.Get("/addStream", handler)

	app.Listen(":3000")
}
