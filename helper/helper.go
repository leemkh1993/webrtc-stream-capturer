package helper

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"sync"

	pion "github.com/pion/webrtc/v3"
)

func GetApi(streamerIp string, action string, peerId string, rtspIp string) string {
	switch action {
	case "ice-server":
		return streamerIp + "/api/getIceServers"
	case "call":
		return streamerIp + "/api/call?peerid=" + peerId + "&url=" + url.QueryEscape(rtspIp) + "&options=" + url.QueryEscape("rtptransport=tcp&timeout=60")
	case "hang-up":
		return streamerIp + "/api/hangup?peerid=" + peerId
	case "get-ice-cand":
		return streamerIp + "/api/getIceCandidate?peerid=" + peerId
	case "add-ice-cand":
		return streamerIp + "/api/addIceCandidate?peerid=" + peerId
	default:
		return ""
	}
}

func GetIceServers(api string) pion.Configuration {
	var syncWG sync.WaitGroup
	channel := make(chan pion.Configuration)

	syncWG.Add(1)
	go func() {
		response, err := http.Get(api)
		IsError("Func get ice servers", err)
		defer response.Body.Close()

		body, err := ioutil.ReadAll(response.Body)
		IsError("Func get ice servers", err)

		resJson := pion.Configuration{}
		json.Unmarshal(body, &resJson)

		syncWG.Done()
		channel <- resJson
	}()

	syncWG.Wait()

	return <-channel
}

func SendOfferToStreamer(api string, offer pion.SessionDescription) (answer pion.SessionDescription) {
	channel := make(chan pion.SessionDescription)

	var syncWG sync.WaitGroup
	syncWG.Add(1)

	go func() {
		log.Println("Sending offer to streamer: " + offer.Type.String())
		bodyJson, err := json.Marshal(offer)
		IsError("JSON Marshal error: ", err)

		response, err := http.Post(api, "application/json;charset=UTF-8", bytes.NewBuffer(bodyJson))
		IsError("Post offer to streamer error: ", err)
		defer response.Body.Close()

		resBody, err := ioutil.ReadAll(response.Body)

		IsError("Offer response body read all error: ", err)

		answerJson := pion.SessionDescription{}

		err = json.Unmarshal(resBody, &answerJson)
		IsError("Answer unmarshal error: ", err)

		syncWG.Done()
		channel <- answerJson
	}()

	syncWG.Wait()
	return <-channel
}

func AddIceCandidate(api string, iceCandidate *pion.ICECandidate) error {
	iceCandidateJson := iceCandidate.ToJSON()
	reqBody, err := json.Marshal(iceCandidateJson)
	IsError("Func add ice candidate: ", err)
	response, err := http.Post(api, "application/json;charset=UTF-8", bytes.NewBuffer(reqBody))
	IsError("Func add ice candidate: ", err)
	defer response.Body.Close()
	resBody, err := ioutil.ReadAll(response.Body)

	IsError("Func add ice candidate: ", err)

	log.Println("Add ice candidate to streamer status: ", string(resBody))

	return nil
}

func AddCodec(mediaEngine *pion.MediaEngine, codec pion.RTPCodecCapability, payload pion.PayloadType) {
	err := mediaEngine.RegisterCodec(
		pion.RTPCodecParameters{
			RTPCodecCapability: codec,
			PayloadType:        payload,
		},
		pion.RTPCodecTypeVideo,
	)
	IsError("Func add codec: ", err)
}

func IsError(prefix string, err error) {
	if err != nil {
		log.Fatalln(prefix, err)
	}
}
