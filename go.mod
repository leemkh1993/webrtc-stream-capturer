module gitlab.com/leemkh1993/webrtc-streamer

go 1.16

require (
	github.com/gofiber/fiber/v2 v2.24.0
	github.com/google/uuid v1.3.0
	github.com/pion/mediadevices v0.3.2
	github.com/pion/webrtc/v3 v3.1.17
	golang.org/x/crypto v0.0.0-20220112180741-5e0467b6c7ce // indirect
	golang.org/x/image v0.0.0-20211028202545-6944b10bf410 // indirect
	golang.org/x/net v0.0.0-20220114011407-0dd24b26b47d // indirect
	golang.org/x/sys v0.0.0-20220114195835-da31bd327af9 // indirect
)
