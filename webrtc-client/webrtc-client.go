package WebRtcClient

import (
	"errors"
	"log"

	uuid "github.com/google/uuid"
	_ "github.com/pion/mediadevices/pkg/driver/videotest"
	pion "github.com/pion/webrtc/v3"
	helper "gitlab.com/leemkh1993/webrtc-streamer/helper"
)

type WebRtcClient struct {
	PeerId          string
	StreamerUrl     string
	Url             string
	PcConfig        pion.Configuration
	Pc              *pion.PeerConnection
	OfferOptions    *pion.OfferOptions
	EarlyCandidates map[string]pion.ICECandidate
	AudioTrack      *pion.TrackRemote
	VideoTrack      *pion.TrackRemote
}

func New(streamerUrl string, rtspUrl string) (client WebRtcClient) {
	client = WebRtcClient{
		StreamerUrl: streamerUrl,
		// Url:         rtspUrl,
		Url: "rtsp://wowzaec2demo.streamlock.net/vod/mp4:BigBuckBunny_115k.mp4",
		OfferOptions: &pion.OfferOptions{
			OfferAnswerOptions: pion.OfferAnswerOptions{
				VoiceActivityDetection: true,
			},
			ICERestart: false,
		},
		EarlyCandidates: make(map[string]pion.ICECandidate),
	}
	return client
}

func (client *WebRtcClient) GetPcConfig() {
	api := helper.GetApi(client.StreamerUrl, "ice-server", client.PeerId, client.Url)
	iceServer := helper.GetIceServers(api)
	client.PcConfig = iceServer
}

func (client *WebRtcClient) CreatePeerConnection() (err error) {
	// Assign a peer id for each connection
	client.PeerId = uuid.New().String()
	// Get peer connection config
	client.GetPcConfig()

	mediaEngine := pion.MediaEngine{}

	googRemb := pion.RTCPFeedback{
		Type: "goog-remb",
	}

	ccmFir := pion.RTCPFeedback{
		Type: "ccm fir",
	}

	transportCC := pion.RTCPFeedback{
		Type: "transport-cc",
	}

	nack := pion.RTCPFeedback{
		Type: "nack",
	}

	nackPli := pion.RTCPFeedback{
		Type: "nack pli",
	}

	// payload type: 102, 127, 125, 108, 124, 123,
	h264Codec := pion.RTPCodecCapability{
		MimeType:     pion.MimeTypeH264,
		ClockRate:    90000,
		Channels:     0,
		SDPFmtpLine:  "level-asymmetry-allowed=1;packetization-mode=1;",
		RTCPFeedback: []pion.RTCPFeedback{googRemb, nack, nackPli, transportCC, ccmFir},
	}

	// payload type: 35
	av1Codec := pion.RTPCodecCapability{
		MimeType:     pion.MimeTypeAV1,
		ClockRate:    90000,
		Channels:     0,
		SDPFmtpLine:  "",
		RTCPFeedback: []pion.RTCPFeedback{googRemb, nack, nackPli, transportCC, ccmFir},
	}

	// payload type: 96
	vp8Codec := pion.RTPCodecCapability{
		MimeType:     pion.MimeTypeVP8,
		ClockRate:    90000,
		Channels:     0,
		SDPFmtpLine:  "",
		RTCPFeedback: []pion.RTCPFeedback{googRemb, nack, nackPli, transportCC, ccmFir},
	}

	// payload type: 100, 98, 122,
	vp9Codec := pion.RTPCodecCapability{
		MimeType:     pion.MimeTypeVP9,
		ClockRate:    90000,
		Channels:     0,
		SDPFmtpLine:  "",
		RTCPFeedback: []pion.RTCPFeedback{googRemb, nack, nackPli, transportCC, ccmFir},
	}

	codecs := [4]pion.RTPCodecCapability{h264Codec, vp8Codec, vp9Codec, av1Codec}

	for _, codec := range codecs {
		switch codec.MimeType {
		case pion.MimeTypeAV1:
			for _, payload := range [1]pion.PayloadType{35} {
				helper.AddCodec(&mediaEngine, codec, payload)
			}
		case pion.MimeTypeH264:
			for _, payload := range [6]pion.PayloadType{102, 127, 125, 108, 124, 123} {
				helper.AddCodec(&mediaEngine, codec, payload)
			}
		case pion.MimeTypeVP8:
			for _, payload := range [1]pion.PayloadType{96} {
				helper.AddCodec(&mediaEngine, codec, payload)

			}
		case pion.MimeTypeOpus:
			for _, payload := range [1]pion.PayloadType{111} {
				helper.AddCodec(&mediaEngine, codec, payload)
			}
		case pion.MimeTypeVP9:
			for _, payload := range [3]pion.PayloadType{100, 98, 122} {
				helper.AddCodec(&mediaEngine, codec, payload)
			}
		default:
			continue
		}
	}

	// Create API object with media engine
	api := pion.NewAPI(pion.WithMediaEngine(&mediaEngine))

	// Create new pc
	pionPc, err := api.NewPeerConnection(client.PcConfig)

	helper.IsError("Func create peer connection: ", err)

	pionPc.AddTransceiverFromKind(pion.RTPCodecTypeVideo, pion.RTPTransceiverInit{
		Direction:     pion.RTPTransceiverDirectionRecvonly,
		SendEncodings: []pion.RTPEncodingParameters{},
	})

	// On ice candidate event
	pionPc.OnICECandidate(client.onIceCandidate)

	// On track event
	pionPc.OnTrack(client.onTrack)

	// On connection state change event
	pionPc.OnConnectionStateChange(client.onConnectionStateChange)

	pionPc.OnICEGatheringStateChange(client.onIceGatheringStateChange)

	pionPc.OnNegotiationNeeded(client.onNegotiationNeeded)

	dataChannel, err := pionPc.CreateDataChannel("data", nil)

	helper.IsError("Func create peer connection: ", err)

	dataChannel.OnOpen(client.onDataChannelOpen)

	dataChannel.OnMessage(client.onDataChannelMessage)

	dataChannel.OnClose(client.onDataChannelClose)

	client.Pc = pionPc
	return nil
}

func (client *WebRtcClient) CreateOffer() (offer pion.SessionDescription, err error) {
	if client.Pc == nil {
		return pion.SessionDescription{}, errors.New("peer connection is not found")
	}

	offer, err = client.Pc.CreateOffer(client.OfferOptions)

	return offer, err
}

func (client *WebRtcClient) SetOffer(offer pion.SessionDescription) (err error) {
	if client.Pc == nil {
		return errors.New("peer connection is not found")
	}

	err = client.Pc.SetLocalDescription(offer)
	return err
}

func (client *WebRtcClient) SetAnswer(answer pion.SessionDescription) (err error) {
	if client.Pc == nil {
		return errors.New("peer connection is not found")
	}

	err = client.Pc.SetRemoteDescription(answer)
	return err
}

func (client *WebRtcClient) onIceCandidate(iceCandidate *pion.ICECandidate) {
	if iceCandidate == nil || client.Pc == nil {
		return
	}

	remoteDesc := client.Pc.CurrentRemoteDescription()
	if remoteDesc == nil {
		log.Println("On ice candidate event: no remote desc, adding ice candidate to map ")
		uuid := uuid.New().String()
		client.EarlyCandidates[uuid] = *iceCandidate
	} else {
		log.Println("On ice candidate event: have remote desc, sending all candidates to streamer")
		api := helper.GetApi(client.StreamerUrl, "add-ice-cand", client.PeerId, client.Url)
		for index, earlyCandidate := range client.EarlyCandidates {
			helper.AddIceCandidate(api, &earlyCandidate)
			delete(client.EarlyCandidates, index)
		}
	}
}

func (client *WebRtcClient) onTrack(track *pion.TrackRemote, receiver *pion.RTPReceiver) {
	if track.Kind() == pion.RTPCodecTypeAudio {
		client.AudioTrack = track
		return
	}

	client.VideoTrack = track
}

func (client *WebRtcClient) onConnectionStateChange(connectionState pion.PeerConnectionState) {
	log.Printf("Peer Connection State has changed to: %s \n", connectionState.String())
}

func (client *WebRtcClient) onDataChannelOpen() {
	log.Println("Peer connection data channel: Opened")
}

func (client *WebRtcClient) onDataChannelClose() {
	log.Println("Peer connection data channel: Closed")
}

func (client *WebRtcClient) onIceGatheringStateChange(iceGatherState pion.ICEGathererState) {
	log.Printf("ICE gathering state has changed to: %s \n", iceGatherState)
}

func (client *WebRtcClient) onDataChannelMessage(message pion.DataChannelMessage) {
	log.Printf("Data Channel received msg: %s \n", string(message.Data))
}

func (client *WebRtcClient) onNegotiationNeeded() {
	log.Println("Negotiation needed")
}
